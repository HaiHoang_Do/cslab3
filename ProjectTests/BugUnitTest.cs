//Hai Hoang Do, Kevin Wu
namespace ProjectTests;

[TestClass]
public class BugUnitTest
{
    [TestMethod]
    public void TestMethod1()
    {
    }

    [TestMethod]
    public void SetupGrid_NoException() {
        try {
            MemoryGame.ConcentrationGrid testGrid = new MemoryGame.ConcentrationGrid();
        }
        catch {
            Assert.Fail();
        }
    }

    [TestMethod]
    public void CreateCards_16CardsCreated() {
        MemoryGame.ConcentrationGrid testGrid = new MemoryGame.ConcentrationGrid();

        List<MemoryGame.Card> testList = testGrid.CreateCards();

        Assert.AreEqual(16, testList.Count);
    }

    [TestMethod]
    public void AddPoint_PlayerAddedPoint() {
        MemoryGame.Player testPlayer = new MemoryGame.Player("test");

        testPlayer.addPoint();
        Assert.AreEqual(1, testPlayer.Points);
    }
}