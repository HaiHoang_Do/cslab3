//Hai Hoang Do, Kevin Wu
namespace ProjectTests;

[TestClass]
public class GridUnitTest
{
    [TestMethod]
    public void TestSetupGrid(){
        //shuffled grid
        MemoryGame.ConcentrationGrid testGoodGrid = new MemoryGame.ConcentrationGrid();
        testGoodGrid.SetupGrid();

        //unshuffled grid
        MemoryGame.ConcentrationGrid testBadGrid = new MemoryGame.ConcentrationGrid();
        testBadGrid.CreateCards();

        bool NotTheSame = false;

        //check every space on the grid, if even 1 doesn't match return that 'true' grid was shuffled
        for(int i = 0; i < 4; i++){
            for(int j = 0; j < 4; j++){
                if(testGoodGrid._grid[i, j] != testBadGrid._grid[i, j]){
                    NotTheSame = true;
                    break;
                }
            } if(NotTheSame){
                break;
            }
        }
        Assert.IsTrue(NotTheSame);

    }

    [TestMethod]
    public void TestCreateCards_16Cards(){
        MemoryGame.ConcentrationGrid testGrid = new MemoryGame.ConcentrationGrid();

        List<MemoryGame.Card> testDeck = testGrid.CreateCards();

        //should be 16 card (4x4 = 16)
        Assert.AreEqual(16, testDeck.Count);
    }

    [TestMethod]
    public void TestCreateCards_NotNull(){
        MemoryGame.ConcentrationGrid testGrid = new MemoryGame.ConcentrationGrid();

        List<MemoryGame.Card> testDeck = testGrid.CreateCards();

        //not empty/null
        Assert.IsNotNull(testDeck);
    }

    [TestMethod]
    public void TestCreateCards_VerifyingGoodCards(){
        MemoryGame.ConcentrationGrid testGrid = new MemoryGame.ConcentrationGrid();

        List<MemoryGame.Card> testDeck = testGrid.CreateCards();

        //making sure theres 2 of each card enum in the testDeck
        foreach(MemoryGame.CardValue value in Enum.GetValues(typeof(MemoryGame.CardValue))){
            Assert.AreEqual(2, testDeck.Count(card => card.Value == value));
        }
    }

    [TestMethod]
    public void TestFillGrid_With16Cards(){
        MemoryGame.ConcentrationGrid testGrid = new MemoryGame.ConcentrationGrid();
        List<MemoryGame.Card> testDeck = testGrid.CreateCards();

        testGrid.FillGrid(testDeck);
        int filledAmount = 0;
        for(int i=0; i<4; i++){
            for(int j=0; j<4; j++){
                if(testGrid._grid[i, j] != null){
                    filledAmount++;
                }
            }
        }

        Assert.AreEqual(16, filledAmount);
    }

    [TestMethod]
    public void TestFillGrid_LessThan16Cards(){
        MemoryGame.ConcentrationGrid testGrid = new MemoryGame.ConcentrationGrid();
        List<MemoryGame.Card> testDeck = new List<MemoryGame.Card>
        {
            //1 card < 16 cards
            new MemoryGame.Card(MemoryGame.CardValue.A)
        };

        Assert.ThrowsException<ArgumentOutOfRangeException>(() => testGrid.FillGrid(testDeck));
    }

    [TestMethod]
    public void TestFillGrid_MoreThan16Cards(){
        MemoryGame.ConcentrationGrid testGrid = new MemoryGame.ConcentrationGrid();

        List<MemoryGame.Card> testDeck = new List<MemoryGame.Card>();
        foreach (MemoryGame.CardValue val in Enum.GetValues<MemoryGame.CardValue>())
        {
            //24 cards > 16 cards
            testDeck.Add(new MemoryGame.Card(val));
            testDeck.Add(new MemoryGame.Card(val));
            testDeck.Add(new MemoryGame.Card(val));
        }

        Assert.ThrowsException<ArgumentOutOfRangeException>(() => testGrid.FillGrid(testDeck));
    }

    [TestMethod]
    public void CheckMatch_ReturnTrue() {
        MemoryGame.ConcentrationGrid testGrid = new MemoryGame.ConcentrationGrid();
        List<MemoryGame.Card> testList = testGrid.CreateCards();
        int ListIndex = 0;

        //Puts the deck in alphabetical order
        for(int i = 0; i < 4; i++) {
            for(int j = 0; j < 4; j++) {
                testGrid._grid[i,j] = testList[ListIndex];
                ListIndex += 1;
            }
        }

        Assert.IsTrue(testGrid.CheckMatch(new Tuple<int, int>(0,0), new Tuple<int, int>(0,1)));
     }

     [TestMethod]
    public void CheckMatch_ReturnFalse() {
        MemoryGame.ConcentrationGrid testGrid = new MemoryGame.ConcentrationGrid();
        List<MemoryGame.Card> testList = testGrid.CreateCards();
        int ListIndex = 0;

        //Puts the deck in alphabetical order
        for(int i = 0; i < 4; i++) {
            for(int j = 0; j < 4; j++) {
                testGrid._grid[i,j] = testList[ListIndex];
                ListIndex += 1;
            }
        }

        Assert.IsFalse(testGrid.CheckMatch(new Tuple<int, int>(0,1), new Tuple<int, int>(0,2)));
     }

     [TestMethod]
     public void IsEmpty_ReturnTrue() {
        MemoryGame.ConcentrationGrid testGrid = new MemoryGame.ConcentrationGrid();

        //Empties the grid by making everything null
        for(int i = 0; i < 4; i++) {
            for(int j = 0; j < 4; j++) {
                testGrid._grid[i,j] = null;
            }
        }

        Assert.IsTrue(testGrid.IsEmpty());
     }

    [TestMethod]
     public void IsEmpty_ReturnFalse() {
        MemoryGame.ConcentrationGrid testGrid = new MemoryGame.ConcentrationGrid();

        //Empties the grid by making everything null except for the last row;
        for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 4; j++) {
                testGrid._grid[i,j] = null;
            }
        }

        Assert.IsFalse(testGrid.IsEmpty());
     }

}