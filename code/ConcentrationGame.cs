namespace MemoryGame
{
    public class ConcentrationGame
    {

        private ConcentrationGrid _grid;
        private Player _activePlayer;
        private Player _inactivePlayer;
        public static void Main(string[] args)
        {
            ConcentrationGame game = new ConcentrationGame();
            game.RunGame();
        }


        //The constructor sets up a new game of Concentration
        public ConcentrationGame()
        {
            _grid = new ConcentrationGrid();
            Console.WriteLine("Welcome to Concentration, the matching game!");
            Console.WriteLine("Player 1, enter your name:");
            _activePlayer = new Player(GetValidInput());
            Console.WriteLine("Player 2, enter your name:");
            _inactivePlayer = new Player(GetValidInput());
        }

        //Get non-null input
        private string GetValidInput()
        {
            string? input;
            do
            {
                input = Console.ReadLine();
            } while (input == null);
            return input;
        }


        private void RunGame()
        {
            while (!_grid.IsEmpty())
            {
                Boolean madeMatch = DoTurn(_activePlayer);
                if (madeMatch)
                {
                    _activePlayer.addPoint();
                    Console.WriteLine($"{_activePlayer.Name} made a match! They now have {_activePlayer.Points} points");
                }
                else
                {
                    Console.WriteLine($"Sorry, no match! It is now {_inactivePlayer.Name}'s turn.");
                    SwitchActivePlayer();
                }
                Console.ReadKey();
            }
            ShowResults();
        }

        internal Boolean DoTurn(Player p)
        {
            //Create List of Guesses;
            List<Tuple<int, int>> guesses = new List<Tuple<int, int>>();
            //Ask for first guess, add it to the list
            guesses.Add(GetGuess(p, guesses));
            //Ask for second guess, add it to the list
            guesses.Add(GetGuess(p, guesses));
            bool _validSecondGuess = false;
            while(!_validSecondGuess) {
                if (guesses[0].Equals(guesses[1])) {
                    guesses[1] = GetGuess(p, guesses);
                }
                else {
                    _validSecondGuess = true;
                }
            }
            Console.Clear();
            _grid.PrintGrid(guesses);
            //Check to see if it Matches, return the result
            return _grid.CheckMatch(guesses[0], guesses[1]);
        }

        //Prompt the user to enter a guess.
        internal Tuple<int, int> GetGuess(Player p, List<Tuple<int, int>> guesses)
        {
            Console.Clear();
            Console.WriteLine($"{p.Name}, choose a row and column to guess");
            _grid.PrintGrid(guesses);
            int row = 0;
            int column = 0;
            bool rowIsValid = false;
            while(!rowIsValid){
                Console.Write("Row: ");
                row = Convert.ToInt32(GetValidInput());
                if(row <= 3 && row >= 0){
                    rowIsValid = true;
                }else{
                    Console.Write("Invalid, chose an existing row");
                }
            }
            bool colIsValid = false;
            while(!colIsValid){
                Console.Write("Col: ");
                column = Convert.ToInt32(GetValidInput());
                if(column <= 3 && column >= 0){
                    colIsValid = true;
                }else{
                    Console.Write("Invalid, chose an existing column");
                }
            }
            return new Tuple<int, int>(row, column);
        }

        //Swaps the currently active player, making the inactive player active and vice versa
        internal void SwitchActivePlayer()
        {
            Player _tempPlayer = _activePlayer;
            _activePlayer = _inactivePlayer;
            _inactivePlayer = _tempPlayer;
        }

        private void ShowResults()
        {
            Console.Clear();
            Console.WriteLine($"Game over!");
            Console.WriteLine($"{_activePlayer.Name} has {_activePlayer.Points} points");
            Console.WriteLine($"{_inactivePlayer.Name} has {_inactivePlayer.Points} points");
            if (_activePlayer.Points > _inactivePlayer.Points)
            {
                Console.WriteLine($"{_activePlayer.Name} wins!");
            }
            else
            {
                Console.WriteLine($"{_inactivePlayer.Name} wins!");
            }
        }
    }
}